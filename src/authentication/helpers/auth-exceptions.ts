export enum AuthExceptions {
  userAlreadyExist = 'User with that email already exists',
  wrongCredentials = 'Wrong credentials provided',
}
