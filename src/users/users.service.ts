import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import User from './user.entity';
import CreateUserDto from './dto/create-user.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  //for test purposes only
  async getByEmail(email: string): Promise<User> {
    const user = await this.usersRepository.findOne({ email });
    if (user) {
      return user;
    }
    throw new HttpException(
      'User with this email does not exist',
      HttpStatus.NOT_FOUND,
    );
  }

  async getUserNamesById(idNumbers: number[]): Promise<string[]> {
    const userNames: string[] = [];
    for (const id of idNumbers) {
      const userName = await this.usersRepository.findOne({ id });
      userNames.push(userName.name);
    }
    return userNames;
  }

  getAllUsers(): Promise<User[]> {
    return this.usersRepository.find();
  }

  async createUser(userData: CreateUserDto): Promise<User> {
    userData.income = 0;
    userData.outcome = 0;
    const newUser = await this.usersRepository.create(userData);
    await this.usersRepository.save(newUser);
    return newUser;
  }

  async getById(id: number): Promise<number> {
    const user = await this.usersRepository.findOne({ id });
    if (user) {
      return user.id;
    }
    throw new HttpException(
      'User with this id does not exist',
      HttpStatus.NOT_FOUND,
    );
  }

  async getIdByEmail(email: string): Promise<number> {
    const user = await this.usersRepository.findOne({ email });
    if (user) {
      return user.id;
    }
    throw new HttpException(
      'User with this id does not exist',
      HttpStatus.NOT_FOUND,
    );
  }
}
