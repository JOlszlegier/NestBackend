import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { ApiOkResponse } from '@nestjs/swagger';
import User from './user.entity';

@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @Post()
  @ApiOkResponse({
    description: 'User created',
  })
  createUser(@Body() body: CreateUserDto): Promise<User> {
    return this.usersService.createUser(body);
  }

  @Get('/all')
  @ApiOkResponse({
    description: 'All users',
  })
  getUsers(): Promise<User[]> {
    return this.usersService.getAllUsers();
  }

  @Get('/email')
  @ApiOkResponse({ description: 'User data with given email' })
  getUserByEmail(@Body() body: { email: string }): Promise<User> {
    return this.usersService.getByEmail(body.email);
  }
}
